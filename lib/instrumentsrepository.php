<?php
include_once('jsonstorage.php');
class InstrumentsRepository extends JsonStorage {
    public function __construct() {
        // paraméterben meg kell adni, hogy hol van a fájlunk.
        parent::__construct('db/instruments.json');
    }

    /**
     * Megkeresi az $id azonosítóhoz tartozó instrument nevét.
     * @param mixed $id - ez string vagy int, vagy time attól függően, hogy az adatszerkezetünkben mit kért a feladat.
     * @return string Keresett instrument neve.
     */
    public function getInstrumentsName($id){
        // $inst-be bekészítjük az összes találatot.
        // indítunk egy szűrést az adatokra ezt elnevvezük mondjuk $row-nak, vagy $adatnak, és közben felhasználjuk a paraméterként kapott $id-t
        $inst = $this->filter(function($row) use($id){
            // ha az aktuális sor [id] -ja megegyezik a paraméter ID-val akkor visszatér ezzel a sorral a fügyvény.
            return $row['id'] === $id;
        });

        // return $inst[key($inst)]['name']; // ha esetleg nem objektummal dolgoznánk, akkor a visszatérő tömb array[X] => keresett elem, ezt az X-et nem ismerjük, ezért a key($inst)-el visszaszerezzük.
        return $inst[$id]['name'];
    }
}