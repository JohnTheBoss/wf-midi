<?php
/**
 * Visszaadja a Session IF_data-ában tárolt form értékét.
 * 
 * @param  string $key Az input form name-je
 * @return string input értéke, vagy üres string
 */
function getFormData($key)
{
  if (isset($_SESSION['IF_data']) && isset($_SESSION['IF_data'][$key])) {
    return $_SESSION['IF_data'][$key];
  }
  return '';
}

/**
 * Megnézi, hogy létezik-e a $data változóban adat és van-e benne adat.
 * @param $data Változó amit vizsgálunk, hogy van-e benne adat.
 * @return bool ha van benne adat, és nem üres akkor IGAZzal térünk vissza.
 */
function is_empty($data)
{
  return !(isset($data) && trim($data) !== '');
}

/**
 * A $input-ban megadott form elemeket validálja a $rules beállítások alaján, a végeredményt $data és a hibákat a $errors változókba adja vissza.
 * 
 * @param  array $input Validálandó érték, $_POST, $_GET.
 * @param array $rules értéke, vagy üres string.
 * @param array $data Ebbe a változóban fognak visszaérkezni az input adatok.
 * @param array $errors Ebbe a változóba érkeznek vissza a hiba üzenet(ek). 
 * 
 * @return bool true -> ha valid, false -> nem valid az input
 */
function isvalid($input, $rules, &$data, &$errors)
{
  $data = [];
  $errors = [];

  foreach ($rules as $rule) {
    $opt = null;
    
    // ha nem üres a bemenet, elkezdjük feldolgozni.
    if (!is_empty($input[$rule['key']])) {
      if (isset($rule['options'])) {
        $opt = array("options" => $rule['options']);
      }
      // Ha nem valid, akkor...
      if (!filter_var($input[$rule['key']], $rule['filter'], $opt)) {
        // Ha van default értékem akkor beállíjuk, feltételezzük, hogy az helyes!
        if (isset($rule['default'])) {
          $data[$rule['key']] = $rule['default'];
        }
        // Hibaüzenet megjelenítés.
        if (isset($rule['errormsg'])) {
          $errors[$rule['key']] = $rule['errormsg'];
        } else {
          $errors[$rule['key']] = "{" . $rule['key'] . "} hiányzik!";
        }
      } else {
        $data[$rule['key']] = $input[$rule['key']];
      };
    } else {
      // ha üres, és van requiredmsg-ben tartalom, akkor azzal a hibaüzenettel térünk vissza.
      if (isset($rule['requiredmsg'])) {
        $errors[$rule['key']] =  $rule['requiredmsg'];
      } else {
        // ha nincs megnézzük van-e errormsg és azt adjuk vissza.
        if (isset($rule['errormsg'])) {
          $errors[$rule['key']] = $rule['errormsg'];
        } else {
          // ha ez sincs akkor visszatérünk egy álltalános hibaüzenettel.
          $errors[$rule['key']] = "{" . $rule['key'] . "} hiányzik!";
        }
      }
    }
  }
  // ha nincs error, akkor sikeres a validálás.
  return count($errors) == 0;
}
