<?php
include_once('jsonstorage.php');

class TrackRepository extends JsonStorage {
    public function __construct() {
        // paraméterben meg kell adni, hogy hol van a fájlunk.
        parent::__construct('db/tracks.json');
    }
}