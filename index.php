<?php
// meghívjuk a két osztályunkat, amik a fájlokat tárolják, és létrehozzuk őket.
require_once('lib/trackrepository.php');
require_once('lib/instrumentsrepository.php');
$tracks = new TrackRepository();
$instruments = new InstrumentsRepository();

// segédosztályoknak van egy all() függvénye ami visszaadja az összes elemet, ezt fogjuk majd később használni.

$hide = [];
if (isset($_GET['hide'])) {
  // feladat leírása szerint ,-jel el vannak választva miket kell elrejteni.
  $hide = explode(',', $_GET['hide']);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>MIDI editor</title>
  <link rel="stylesheet" href="http://webprogramozas.inf.elte.hu/webprog/zh/midi/midi.css">
</head>

<body>
  <div id="main">
    <div class="row tracks-container">
      <?php if (!in_array("tracks", $hide)) : ?>
        <div class="tracks">
          <h3>MIDI editor</h3>
          <p>Press 1-8 to play notes, and SPACE to toggle record mode</p>
          <a href="new.php">Add new track...</a>
          <ul>
            <?php foreach ($tracks->all() as $track) : ?>
              <li style="background-color: <?= $track['color']; ?>" data-id="<?= $track['id']; ?>" data-notes='<?= json_encode($track['notes']); //itt a stringből JSON-t kell csinálni ?>'>
                <span><?= $track['category']; ?></span>
                <?= $track['name']; ?> (<?= $instruments->getInstrumentsName($track['instrument']); // az általunk írt függvénnyel, megszerezzük a nevét ?>)
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endif; ?>

      <div class="pianoroll-container">

        <?php if (!in_array("pianoroll", $hide)) : // Ha $hide tömbben van ilyen elem akkor az egész mindenséget el kell rejteni. ?>
          <div class="pianoroll">
            <div class="row">
              <div class="notes">
                <div>C+</div>
                <div>B</div>
                <div>A</div>
                <div>G</div>
                <div>F</div>
                <div>E</div>
                <div>D</div>
                <div>C</div>
              </div>
              <svg viewBox="0 0 10000 80" width="100%" height="200" preserveAspectRatio="none">
              </svg>
            </div>
            <textarea></textarea>
            <button id="savebutton">Save to selected track <span></span></button>
            <button id="showbutton">Show JSON in SVG</button>
          </div>
        <?php endif; ?>
        <?php if (!in_array("keyboard", $hide)) :  // Ha $hide tömbben van ilyen elem akkor az egész mindenséget el kell rejteni.  ?>
          <div class="keyboard">
            <div>C<span>1</span></div>
            <div>D<span>2</span></div>
            <div>E<span>3</span></div>
            <div>F<span>4</span></div>
            <div>G<span>5</span></div>
            <div>A<span>6</span></div>
            <div>B<span>7</span></div>
            <div>C+<span>8</span></div>
          </div>
        <?php endif; ?>

      </div>

    </div>
  </div>

  <script>
    let selectedNode = "";

    function getNote(type) {
      switch (type) {
        case 'C':
          return 70;
          break;
        case 'D':
          return 60;
          break;
        case 'E':
          return 50;
          break;
        case 'F':
          return 40;
          break;
        case 'G':
          return 30;
        case 'A':
          return 20;
          break;
        case 'B':
          return 10;
          break
        case 'C+':
          return 0;
          break;
      }
    }

    document.getElementById("savebutton").addEventListener("click", function() {
      let span = document.getElementById("savebutton").getElementsByTagName("span")[0];
      span.innerHTML = "";
      if (selectedNode !== "") {
        let textarea = document.getElementsByClassName("pianoroll")[0].getElementsByTagName("textarea")[0];

        // ez lesz a formdata tartalma, notes néven fogunk hivatkozni rá a másik oldalon.
        let formData = {
          notes: JSON.parse(textarea.value)
        }

        // POST-os AJAX + GET metódusban az ID.
        fetch('saveTrack.php?id='+selectedNode.dataset.id, {
            method: 'POST',
            body: JSON.stringify(formData), // Át kell alakítani string-é hogy l tudjuk küldeni, rendesen.
            require: "2xx"
          }).then(response => response.json())
          .then((data) => {
            if(data['status']){ // ha van státusz
              if(data.status === "OK"){ // ami ok
                span.innerHTML = "✔"; // akkor a feladat szerint kell egy pipa
              } else {
                span.innerHTML = "XX"; // ezt nem kéri a feladat, de visszajezünk magunknak, hogy volt valami hiba.
                console.error(JSON.stringify(data)); // kiírjuk a hibát.
              }
            }
          }).catch(err => {
            // ha a fenti then ágat nem tudta a tudtuk kiértékelni, itt iss visszaküldünk egy hibaüzenetet a konzolra.
            console.log(JSON.stringify(err));
          })


      } else {
        span.innerHTML = "Err: tracket ki kell választani!"; // ezt se kérte a feladat, de itt megint magunknak jelezzük, hogy nem választottunk ki semmit és azért nem működik a kód.
      }
    });

    function clearSVG() {
      let svg = document.getElementsByClassName("pianoroll")[0].getElementsByTagName("svg")[0];

      // kitörlünk minden gyereket az SVG-ből. FOR-ral nem megy (darabos, nem törli ki az összeset) de WHILE-al IGEN!
      while (svg.lastChild) {
        svg.removeChild(svg.lastChild);
      }
    }

    function showNotesInSVG() {
      let textarea = document.getElementsByClassName("pianoroll")[0].getElementsByTagName("textarea")[0]; // megkeressük a textareát.
      let notes = JSON.parse(textarea.value); // kiszedjük belőle az adatot
      clearSVG();
      let svg = document.getElementsByClassName("pianoroll")[0].getElementsByTagName("svg")[0]; // megkeressük az SVG-t
      let svgNS = "http://www.w3.org/2000/svg"; // ez kell.
      for (let i = 0; i < notes.length; i++) { // létrehozunk annyi rect-et ahány nodes sorunk van.
        let rect = document.createElementNS(svgNS, "rect"); // itt létrejön a <rect/> csak az üres kellenek bele az adatok.
        rect.setAttributeNS(null, "x", notes[i].start); // az x= "start értékkel"
        rect.setAttributeNS(null, "y", getNote(notes[i].note)); // az y="Magassággal."
        rect.setAttributeNS(null, "width", notes[i].end - notes[i].start); // width pedig a meghatározott értékkel
        rect.setAttributeNS(null, "height", 10); // height is.
        // most már megvan a <rect x="XX" y="XX" width="XX" height="XX" /> értékeint, és most hozzáadjuk az SVG-nkhez.
        svg.appendChild(rect);
      }
    }

    document.getElementById("showbutton").addEventListener("click", () => showNotesInSVG());

    function showNotes() {
      loadNodeData(this);
    }
    let list = document.getElementsByClassName("tracks")[0].getElementsByTagName("ul")[0].getElementsByTagName("li");
    for (let i = 0; i < list.length; i++) {
      list[i].addEventListener("click", showNotes);
    }

    function loadNodeData(elem) {
      // egy változóban tárolom, hogy éppen hol vagyunk, ha ez nem üres, akkor a régi értékről le kell venni a selected-et.
      if (selectedNode !== "") {
        selectedNode.classList.remove("selected");
      }
      // most már egyenlő lehet a jelenlegi értékkel
      selectedNode = elem;

      // hozzáadom hogy ez ki van választva.
      elem.classList.add("selected");
      // megkeressük a textareát, az értéket kiszedjük a data-ÉRTÉKBŐL, és betöltjük a textareába
      let textarea = document.getElementsByClassName("pianoroll")[0].getElementsByTagName("textarea")[0];
      let notes = JSON.parse(elem.dataset.notes); // az elem.dataset.notes ezzel egyenlő data-notes, tehát a dataset.XXX ami a kötőjel után van.
      textarea.innerHTML = JSON.stringify(notes); // a textboxba betöltjük az értéket, DE ide szöveget kell beírni ezért vissza kell alakítani!
      showNotesInSVG(); // SVGket kirajzoljuk.
    }

    // visszaadja hogy éppen hol vagyunk.
    function where() {
      let list = document.getElementsByClassName("tracks")[0].getElementsByTagName("ul")[0].getElementsByTagName("li");
      let r = {
        prev: null,
        next: null
      }

      // egyszerű keresés tétel.
      let i = 0;
      while (i < list.length && list[i] !== selectedNode) {
        i++
      }
      
      if (i < list.length) {
        // ha a 0 helyen vagyunk, akkor a next a következő, a prev pedig az utolsó elem.
        if (i == 0) {
          r.next = i + 1;
          r.prev = list.length - 1; // lenght-el megkapom a tömb hozzát, és -1-el pedig, hogy mi az utolsó elem.
        } else if (i == list.length - 1) { // ha a legvégén vagyunk, akkor a next az első elem, a prev pedig az utolsó elötti
          r.next = 0;
          r.prev = i - 1;
        } else { // minden més esetben a next a +1-es a prev pedig a -1-es érték.
          r.next = i + 1;
          r.prev = i - 1;
        }
      }

      return r;
    }

    let rec = false;
    let recTime = 0;
    let PianoKeyStart = [0, 0, 0, 0, 0, 0, 0];

    let recNodes = [];

    // új elem hozzáadása, hangjegy, felvétel kezdete, és a gomb lenyomásának kezdete.
    function addNewNode(note, recStart, keyStart) {
      let delta = (new Date()).getTime() - keyStart; // gomb felengedés ideje MOST - kezdete
      let start = (keyStart - recStart); // gomb lenyomva - felvétel start
      let end = start + delta; // Start idő + vége.

      recNodes.push({
        note: note,
        start: start,
        end: end
      });
    }

    document.addEventListener("keyup", function(e) {
      let list = document.getElementsByClassName("tracks")[0].getElementsByTagName("ul")[0].getElementsByTagName("li");
      // ha nincs kiválaszva semmi, és a felvele nyíl lett megnyomva, akkor az utolsó elem kell nekünk (feladat szerint.)
      if (selectedNode == "" && e.key === "ArrowUp") {
        loadNodeData(list[list.length - 1]);
      } else if (selectedNode == "" && e.key === "ArrowDown") {
        // ha nincs kiválaszva semmi, és a lefele nyíl lett megnyomva, akkor az első elem kell nekünk (feladat szerint.)
        loadNodeData(list[0]);
      } else {
        // minden más esetben meg kell nézni, ki az előző és a következő elem.
        if (e.key === "ArrowUp") {
          // kell az előző elem.
          let before = where();
          loadNodeData(list[before.prev]);
        } else if (e.key === "ArrowDown") {
          // kell a következő elem.
          let after = where();
          loadNodeData(list[after.next]);
        }
      }

      let keyboard = document.getElementsByClassName("keyboard")[0].getElementsByTagName("div");

      // mindehonnan leszedjük az aktivitást, és hozzáadjuk a hangot, a listához, és nullzuk az időt.
      switch (e.code) {
        case "Digit1":
          keyboard[0].classList.remove("active"); // aktivitás levesz
          addNewNode("C", recTime, PianoKeyStart[0]); // hozzáad a listához
          PianoKeyStart[0] = 0; // idő nullázás
          break;
        case "Digit2":
          keyboard[1].classList.remove("active");
          addNewNode("D", recTime, PianoKeyStart[1]);
          PianoKeyStart[1] = 0;
          break;
        case "Digit3":
          keyboard[2].classList.remove("active");
          addNewNode("E", recTime, PianoKeyStart[2]);
          PianoKeyStart[2] = 0;
          break;
        case "Digit4":
          keyboard[3].classList.remove("active");
          addNewNode("F", recTime, PianoKeyStart[3]);
          PianoKeyStart[3] = 0;
          break;
        case "Digit5":
          keyboard[4].classList.remove("active");
          addNewNode("G", recTime, PianoKeyStart[4]);
          PianoKeyStart[4] = 0;
          break;
        case "Digit6":
          keyboard[5].classList.remove("active");
          addNewNode("A", recTime, PianoKeyStart[5]);
          PianoKeyStart[5] = 0;
          break;
        case "Digit7":
          keyboard[6].classList.remove("active");
          addNewNode("B", recTime, PianoKeyStart[6]);
          PianoKeyStart[6] = 0;
          break;
        case "Digit8":
          keyboard[7].classList.remove("active");
          addNewNode("C+", recTime, PianoKeyStart[7]);
          PianoKeyStart[7] = 0;
          break;
      }
    });

    document.addEventListener("keydown", function(e) {

      if (e.code == "Space") {
        let svg = document.getElementsByClassName("pianoroll")[0].getElementsByTagName("svg")[0];
        if (rec) { // ha ez true akkor most le kell állítani a felvételt.
          recTime = 0; // start időt nullázuk.
          svg.classList.remove("active"); // leszedjük, hogy aktív
          let textarea = document.getElementsByClassName("pianoroll")[0].getElementsByTagName("textarea")[0];
          textarea.value = JSON.stringify(recNodes); // kiírjuk az eredményt, és megjelenítjük
          showNotesInSVG();
          rec = false; // átállítjuk, hogy most már nem rögzítünk
        } else {
          // ha false akkor el kell indítani a felvételt, rögzítjuk a start időt, beállítjuk hogy felvétel van, és nullázuk a felvétel tartalmát.
          svg.classList.add("active");
          recTime = (new Date()).getTime(); // ez ms-ben visszaadja az 1970-01-01 óta eltelt időt. SQLben, és sok helyen TimeStamp-ként, Unix TimeStamp szokták emlegetni.
          rec = true;
          recNodes = [];
        }
      }

      let keyboard = document.getElementsByClassName("keyboard")[0].getElementsByTagName("div");

      // minden gombot aktívra állít ha kell, és rögzíti a lenyomás dátumát.
      switch (e.code) {
        case "Digit1":
          keyboard[0].classList.add("active");
          if (!PianoKeyStart[0]) { // csak akkor rögzítjük ha 0 van benne, különben folyamatosan felülírnánk az egészet.
            PianoKeyStart[0] = (new Date()).getTime();
          }
          break;
        case "Digit2":
          keyboard[1].classList.add("active");
          if (!PianoKeyStart[1]) {
            PianoKeyStart[1] = (new Date()).getTime();
          }
          break;
        case "Digit3":
          keyboard[2].classList.add("active");
          if (!PianoKeyStart[2]) {
            PianoKeyStart[2] = (new Date()).getTime();
          }
          break;
        case "Digit4":
          keyboard[3].classList.add("active");
          if (!PianoKeyStart[3]) {
            PianoKeyStart[3] = (new Date()).getTime();
          }
          break;
        case "Digit5":
          keyboard[4].classList.add("active");
          if (!PianoKeyStart[4]) {
            PianoKeyStart[4] = (new Date()).getTime();
          }
          break;
        case "Digit6":
          keyboard[5].classList.add("active");
          if (!PianoKeyStart[5]) {
            PianoKeyStart[5] = (new Date()).getTime();
          }
          break;
        case "Digit7":
          keyboard[6].classList.add("active");
          if (!PianoKeyStart[6]) {
            PianoKeyStart[6] = (new Date()).getTime();
          }
          break;
        case "Digit8":
          keyboard[7].classList.add("active");
          if (!PianoKeyStart[7]) {
            PianoKeyStart[7] = (new Date()).getTime();
          }
          break;
      }
    });
  </script>

</body>

</html>