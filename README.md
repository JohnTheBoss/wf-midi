# WebFejlesztés Midi ZH
Midi ZH feladatsor megoldása.
## Melyik JSON fájlokat használtam?
A feladat megoldásához a `*.object.json` fájlokat használtam fel.
# Tartalom
-  [Fájlkezelés](#fájlkezelés)
	- [Használata](#így-használd)
	- [Összes elem lekérdezése](#összes-elem-lekérdezése-a-repository-ból)
	- [Szűrés](#egy-adott-elem-lekérdezése-filter)
	- [Új elem](#új-elem-beszúrása-insert)
	- [Meglévő frissítése / módosítása](#elem-szerkesztése-update)
	- [Elem törlése](#elem-törlése-delete)
	- [Saját függvény](#egyedi-függvény-írás-a-repository-ba)
-  [Form validálás](#form-validálás)
	- [Kötelező szöveges mező](#szöveges-kötelező-mező)
	- [Csak szám lehet és kötelező mező](#csak-számot-tartalmazhat-és-kötelező-mező-különböző-hibaüzenetek-kellenek)
	- [Szám intervallum között](#csak-számot-tartalmazhatt-és-1-100-között-kell-lennie)
	- [3 felétel, 3 különböző hibaüzenettel](#csak-számot-tartalmazzhat-kötelező-mező-1-100-között-kell-lennie)
	- [REGEX](#regex)
	- [Egyedi feltételek](#egyedi-feltételek)
- [Validálás feldolgozása, mentés elött adatok módosítása, törlése](#validálás-követően-adat-módosítás-szükségtelen-adatt-törlése)

# Hasznos:
[További PHP validátor típusok](https://www.php.net/manual/en/filter.filters.validate.php)
# Fájlkezelés:
Fájlkezeléshez, az előadó álltal bemutatott `jsonio.php` és a `jsonstorage.php` fájlokat használtam fel. Azzal a különbséggel, hogy a jsonstorage.php-ban az **_id** mezőt simán **id**-ra cseréltem.
Ezeket a fájlokat megtalálod a **lib** mappában.
### Így használd:
A **DB** mappában vannak az object fájlok.
1. Hozz létre a **lib** mappában egy új PHP fájlt mondjuk `FAJLNEVRepository.php` néven.
2. Ennek a tartalma legyen a következő:
```php
<?php
include_once('jsonstorage.php');
class FAJLNEVRepository extends JsonStorage {
	public  function  __construct() {
		// paraméterben meg kell adni, hogy hol van a fájlunk.
		parent::__construct('db/FAJLNEV.json');
	}
}
```
3. Ahol szükséges, mint pl az index.php hívd meg így:
```php
<?php
require_once('lib/FAJLNEVRepository.php');
$FAJLNEV= new  FAJLNEVRepository ();
?>
```
### Összes elem lekérdezése a Repository-ból.
```php
<?php
....
$FAJLNEV= new  FAJLNEVRepository ();
....
$osszes = $FAJLNEV->all();
...
```
### Egy adott elem lekérdezése Filter()
Tegyük fel az `$id`-ban van amit keresek, az adatszerkeszetemben pedig `id: "Keresem"` van.
Ha `var_dump`-al kiírnám a `$talalat`-ot akkor egy tömböt kapnék. Mivel objectes verziót használtuk, így kell nekünk a `$talalat[$id]`-edik elem.
Ha a tömbös verziót használtuk volna akkor a `key($tomb)`-el visszakapnánk az összes elem kulcsát, de mivel csak 1 helyen lenne egyeződés így csak 1 számot kapnánk vissza, akkor az echo sornak így kellene kinéznie: `$talalat[key($talalat)]`
```php
$id = "Keresem";
$talalat = $this->filter(function($row) use($id){
	return  $row['id'] === $id;
});
echo  $talalat[$id]; // végső találat.
```
### Új elem beszúrása Insert()
ID-t automatikusan létrehozz, lásd a `jsonstorage.php`-ban felvan sorolva 2 lehetőség, vagy egy szöveges ID-t hozz létre, vagy egy idő alapú ID-t.
```php
<?php
...
$data = array(
	"nev" => "Elek",
	"kor" => 20,
	"egyetem" => "ELTE IK",
	"baromsag" => "kdfslksdnflsdnflsdf";
);
// ha kéne még valami extra:
$data["extra"] = "Webprog2 javítózik";
unset($data["baromsag"]); // Ha valamelyik input sor esetleg nem kellene. pl pw1 pw2-közül a pw2.
$FAJLNEV->insert($data);
// jöhet az egész $_POST vagy $_GET-ből is,
$FAJLNEV->insert($_POST); // ekkor a teljes POST tartalma bekerül, ilyenkor kellhet 1-2 extra mező.
```
### Elem szerkesztése Update()
Először megkeressük a sort lást első function, majd a 2. functionban, fontos a &-jel!! frissítsük a sor adatait, a fent létrehozott $ujadatok tömbből.
```php
<?php
...
$id = "szövegesID";
$ujadatok = array(
	"kor" => 21,
	"exra" => "webprog2-n átment"
);
$FAJLNEV->update(
	function($sor) use ($id){
		return  $sor['id'] === $id;
	),
	function(&$sor) use ($ujadatok){
		$sor['kor'] = $ujadatok['kor'];
		$sor['extra'] = $ujadatok['extra'];
		... stb...
	}
);
```
### Elem törlése Delete()
Hasonlóképpen működik mint a Filter(), csak ebben az esetben törli az adott sort.
```php
$id = "SzövegesID";
$FAJLNEV->delete(function($sor) use ($id) {
return  $sor['id']===$id;
});
```
### Egyedi függvény írás a Repository-ba
Írhatunk függvényeket, műveleteket közvetlenül a Repositoryba. Lásd `instrumentsrepository.php`
Ezeket úgy hívjuk meg a kódban, hogy ``$FAJLNEV->fuggvenyNeve(esetlegesParamerer/ek);``
PL:
```php
<?= $instruments->getInstrumentsName($track['instrument']);
```
# Form validálás
Form validálásra a `lib/function.php` között található egy, `isvalid()` függvény amivel gyorsan lehet formot validálni.
```php
nev.php
<?php
require_once("lib/function.php");
....
$errors = [];
$data = [];
$input = $_POST;
$rules = [
	[
		"key" => "POST_NEVE", // kötelező
		"filter" => "Valamilyen filter", // kötelező
		"errormsg" => "Hibaüzenetet ha nem valid", // elhagyható
		"requiredmsg" => "Hibaüzenet, ha üres", // elhagyható
		"options" => [...] // kötelező, ha olyan a filter (pl regexp, callback, érték X és Y között)!
	]
];

if(isvalid($input, $rules,$data,$errors)){
	echo  "Valid!"; // $data feldolgozható.
} else {
	var_dump($errors); // hibák...
}

```
### Szöveges kötelező mező
```php
$rules = [
	[
		'key' => 'category',
		'filter' => FILTER_DEFAULT,
		'errormsg' => 'The category is required'
	],
	...
]
```
### Csak számot tartalmazhat és kötelező mező (különböző hibaüzenetek kellenek!)
```php
$rules = [
	[
		'key' => 'instrument',
		'filter' => FILTER_VALIDATE_INT,
		'requiredmsg' => 'The instrument is required',
		'errormsg' => 'The instrument has to be an integer'
	],
	...
]
```
### Csak számot tartalmazhatt és 1-100 között kell lennie
```php
$rules = [
	[
		"key" => "szam",
		'filter' => FILTER_VALIDATE_INT,
		'options' => array('min_range' => 1, 'max_range' => 100),
		'errormsg' => 'Szamnak 1 es 10 kozott kell lennie!'
	],
	...
]
```
### Csak számot tartalmazzhat, kötelező mező, 1-100 között kell lennie
Ha 3 különböző esetett kell vizsgálni, megadhatjuk 3 tömbbös sorban az egészet, így:
```php
$rules = [
	[
		"key" => "szam",
		'filter' => FILTER_DEFAULT,
		'errormsg' => 'A szam mezo kotelezo!'
	],
	[
		"key" => "szam",
		'filter' => FILTER_VALIDATE_INT,
		'errormsg' => 'A szam mezonek szamnak kell lennie!'
	],
	[
		"key" => "szam",
		'filter' => FILTER_VALIDATE_INT,
		'options' => array('min_range' => 1, 'max_range' => 10),
		'errormsg' => 'Szamnak 1 es 10 kozott kell lennie!'
	]
	...
]
```
### REGEX
```php
$rules = [
	[
		"key" => "color",
		'filter' => FILTER_VALIDATE_REGEXP,
		'options' => [
			"regexp" => "/#([a-f0-9]{3}){1,2}\b/i"
		],
		'requiredmsg' => 'The track color is required',
		'errormsg' => 'The track color has a wrong format'
	],
	...
]
```
### Egyedi feltételek
Egyedi szűrést a FILTER_CALLBACK segítségével hozhatunk létre. Ilyenkor az options egy olyan függvényt vár aminek 1 paramétre van. Ebbe a paraméterbe kerül bele a validálandó érték. Ha a validálandó érték helyes, visszatérhetünk egy TRUE-val vagy akár az **értékkel** is! Pl lehet ez a callback egy `strtoupper` is, ami nagybetűsé állítja a validált értéket.
```php
$rules = [
	[
		"key" => "pwd",
		"filter" => FILTER_CALLBACK,
		"errormsg" => "Nem egyezik a két jelszó!",
		"options" => function ($str) use ($input) {
			if ($str == $input['pwd']) {
				return  true;
			} else {
				return  false;
			}
		}
	],
	[
		"key" => "pwd2",
		"filter" => FILTER_CALLBACK,
		"options" => function ($str) use ($input) {
			if ($str == $input['pwd']) {
				return  true;
			} else {
				return  false;
			}
		},
		"errormsg" => "A jelszó2 nem egyezik a jelszó 1-el"
	],
	[
		'key' => "szam2",
		'filter' => FILTER_CALLBACK,
		"options" => function ($ertek) {
			return  $ertek > 1000;
		}
	],
]
```
# Validálás követően adat módosítás, szükségtelen adatt törlése
Ügyeljünk arra ha valami kényes adatot validálunk, mint pl.: jelszó, a validálás után, a feldolgozás elött töröljük a duplikációt, és megfelelően titkosítsuk a jelszót!
```php
<?php
...
if (isvalid($input, $rules, $data, $errors)) {
	unset($data["pwd2"]); // pwd2 törlése
	$data["pwd"] = password_hash($data['pwd'], PASSWORD_DEFAULT); // jelszó hash-elése
	echo  "VALID!";
	var_dump($data);
	$FAJLNEV->insert($data); // rögzítés
} else {
	var_dump($errors);
}
...
```