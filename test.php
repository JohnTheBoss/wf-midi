<?php

require_once("lib/function.php");

$input = [
    "name" => "KUKUCS",
    "color" => "#000",
    "category" => "ABX",
    "instrument" => 4,
    "szam" => 10,
    "szam2" => 1932,
    "pwd" => "Alma",
    "pwd2" => "Alma"
];

$rules = [
    [
        "key" => "pwd",
        "filter" => FILTER_CALLBACK,
        "errormsg" => "Nem egyezik a két jelszó!",
        "options" => function ($str) use ($input) {
            if ($str == $input['pwd']) {
                return true;
            } else {
                return false;
            }
        },
    ],
    [
        "key" => "pwd2",
        "filter" => FILTER_CALLBACK,
        "options" => function ($str) use ($input) {
            if ($str == $input['pwd']) {
                return true;
            } else {
                return false;
            }
        },
        "errormsg" => "A jelszó2 nem egyezik a jelszó 1-el"
    ],
    [
        'key' => "szam2",
        'filter' => FILTER_CALLBACK,
        "options" => function ($ertek) {
            return $ertek > 1000;
        }
    ],
    [
        'key' => "name",
        'filter' => FILTER_DEFAULT,
        'errormsg'  => 'The track name is required',
    ],
    [
        "key" => "color",
        'filter' => FILTER_VALIDATE_REGEXP,
        'options' => [
            "regexp" => "/#([a-f0-9]{3}){1,2}\b/i"
        ],
        'requiredmsg'  => 'The track color is required',
        'errormsg'  => 'The track color has a wrong format'
    ],
    [
        "key" => "category",
        'filter' => FILTER_DEFAULT,
        'errormsg'  => 'The category is required'
    ],
    [
        "key" => "instrument",
        'filter' => FILTER_DEFAULT,
        'errormsg'  => 'The instrument is required'
    ],
    [
        "key" => "instrument",
        'filter' => FILTER_VALIDATE_INT,
        'errormsg'  => 'The instrument has to be an integer'
    ],
    [
        "key" => "szam",
        'filter' => FILTER_DEFAULT,
        'errormsg'  => 'A szam mezo kotelezo!'
    ],
    [
        "key" => "szam",
        'filter' => FILTER_VALIDATE_INT,
        'errormsg'  => 'A szam mezonek szamnak kell lennie!'
    ],
    [
        "key" => "szam",
        'filter' => FILTER_VALIDATE_INT,
        'options'   => array('min_range' => 1, 'max_range' => 10),
        'errormsg'  => 'Szamnak 1 es 10 kozott kell lennie!'
    ]
];

function printArr($arr)
{
    echo "<pre>";
    print_r($arr);
    echo "</pre>";
}

$data = [];
$errors = [];
if (isvalid($input, $rules, $data, $errors)) {
    unset($data["pwd2"]);
    $data["pwd"] = password_hash($data['pwd'], PASSWORD_DEFAULT);
    echo "VALID!";
    printArr($data);
} else {
    printArr($errors);
}
