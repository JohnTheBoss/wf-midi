<?php
// session indítás + segédfüggvények, adatfájlok meghívása.
session_start();

require_once('lib/function.php');
require_once('lib/trackrepository.php');
require_once('lib/instrumentsrepository.php');

$tracks = new TrackRepository();
$instruments = new InstrumentsRepository();

// kinullázza az értékeket ha nincs error.
if(!isset($_GET['error'])){
  $_SESSION['errors'] = [];
  $_SESSION['IF_data'] = [];
}

// ha elküldük a POST-ot és tartalma > 0 akkor elkezdjük feldolgozni.
if (isset($_POST) && count($_POST) > 0) {
  $inputs = $_POST; // kimentjuk az értékeket
  $data = []; // adat tömböt létrehozzuk üresen!

  // felvisszük a szabályokat
  $rules = [
    [
      'key' => "name", // name mező szöveges mező és kötelező.
      'filter' => FILTER_DEFAULT, // megnézi, hogy szöveg van-e benne
      'errormsg'  => 'The track name is required', // requiredmsg-nem szükséges mert mind a két esetben ugyan azt kell visszaküldeni.
    ],
    [
      'key' => "color", // color kötelező mező, csak hexa formátum fogadható el.
      'filter' => FILTER_VALIDATE_REGEXP, // regexp-el ellenőrizzük, hogy jó-e a formátum.
      'requiredmsg' => 'The track color is required', // ha nincs kitöltve akkor ezt kell visszaküldeni,
      'options' => [
        "regexp" => "/#([a-f0-9]{3}){1,2}\b/i"
      ],
      'errormsg'  => 'The track color has a wrong format' // ha kivan, de rossza a formátum akkor ezt.
    ],
    [
      'key' => 'category', // kötelező szöveges mező.
      'filter' => FILTER_DEFAULT,
      'errormsg'  => 'The category is required'
    ],
    [
      'key' => 'instrument', // kötelező mező ami csak számokat tartalmazhat.
      'filter' => FILTER_VALIDATE_INT, // számot tartalmaz??
      'requiredmsg' => 'The instrument is required', // ha nincs kitöltve
      'errormsg'  => 'The instrument has to be an integer' // ha nem számot tartalmazz
    ],
  ];


  $errors = [];
  if (isvalid($inputs, $rules, $data, $errors)) {
    // ha valid akkor errors és mindent 0-ázunk.
      $_SESSION['errors'] = [];
      $_SESSION['IF_data'] = [];
      $data['notes'] = []; // kell egy notes sor is!! ezt állítsuk üres tömbre!
      $tracks->insert($data); // hozzáadja a JSON adatszerkezethez, ami a $data tömbben van.
      header("Location: index.php"); // főoldalra navigálunk.
  } else {
    // ha van hiba, először nullázunk, majd betöltjük az értékeket.
    $_SESSION['errors'] = [];
    $_SESSION['IF_data'] = [];
    $_SESSION['errors'] = $errors;
    $_SESSION['IF_data'] = $inputs;
    header("Location: new.php?error=1"); // visszanavigáljuk ugyan erre az oldalra, de az error=1-el jelezzük, hogy nem volt rendben valami.
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>MIDI editor - Add new track</title>
  <link rel="stylesheet" href="http://webprogramozas.inf.elte.hu/webprog/zh/midi/midi.css">
</head>

<body>
  <h2>Add new track</h2>
  <?php if ($_SERVER["REQUEST_METHOD"] != "POST") : ?>
    <!-- novalidate-al beállíthatjuk, hogy a html ne végezze el az ellenörzést, így te le tudod tesztelni, hogy jó-e a szerver oldali tesztelés -->
    <form action="" method="post" novalidate>
      <div>
        <label for="name">Track name</label>
        <input type="text" id="name" name="name" value="<?= getFormData('name'); // megírt függvény segítségével betöltjük a tartalmat, ha van ?>" required>
        (required)
      </div>
      <div>
        <label for="color">Color</label>
        <!-- itt a type-ot érdemes TEXT-re átírni, ha tesztelni akarod, hogy valid-e és van-e benne tartalom -->
        <input type="color" id="color" name="color" placeholder="#1234af" value="<?= getFormData('color'); ?>" required>
        (required, format: hex color code, e.g. #12af4d)
      </div>
      <div>
        <label for="category">Category</label>
        <input type="text" id="category" name="category" list="category-list" value="<?= getFormData('category'); ?>" required>
        (required)
        <datalist id="category-list">
          <option value="Piano">
          <option value="Organ">
          <option value="Accordion">
          <option value="Strings">
          <option value="Guitar">
          <option value="Bass">
          <option value="Choir">
          <option value="Trumpet">
          <option value="Brass">
          <option value="Saxophone">
          <option value="Flute">
          <option value="Synth Lead">
          <option value="Synth Pad">
          <option value="Percussion">
          <option value="World">
          <option value="Synth effects">
          <option value="Sound effects">
        </datalist>
      </div>
      <div>
        <label for="instrument">Instrument</label>
        <select id="instrument" name="instrument" required>
          <?php foreach ($instruments->all() as $instrument) : // mivel ez egy option rész itt a selected kulcsszót kell a végére tenni, ha éppen az volt korábban kiválasztva ?>
          <option value="<?= $instrument['id']; ?>" <?php if($instrument['id'] == getFormData('instrument')) {echo "selected";}?>><?= $instrument['name']; ?></option>
          <?php endforeach; ?>
        </select>
        (required, number)
      </div>
      <div>
        <button type="submit">Add new track</button>
      </div>
    </form>
  <?php endif; ?>
  <a href="index.php">Return to editor</a>

  <!-- A minta alapján a vissza gomb alatt kell megjelennie a errors divnek. -->

  <?php if (isset($_SESSION['errors']) && count($_SESSION['errors']) > 0) : // ha létezik session errors és elemszáma > 0 akkor kiírjuk?>
    <div class="errors">
      <?php foreach ($_SESSION['errors'] as $error) :?>
      <?= $error; ?>&nbsp;
      <?php endforeach; ?>
    </div>
  <?php endif; ?>

</body>

</html>