<?php
// AJAX-os feldolgozó form
require_once("lib/trackrepository.php");
$tracks = new TrackRepository();

// megnézzük, hogy van-e id, ha van trim-elve megkapom, ha nincs akkor null shorthand IF
$id = isset($_GET['id']) ? trim($_GET['id']) : null;

// a feladat nem kötötte ki, hogy kell validálni, de egy minimálisat validáljunk.
if ($id != null) {

    // megkeressük ID alapján a tracket.
    $track = $tracks->filter(function ($track) use ($id) {
        return $track["id"] === $id;
    });

    // a fetch() body-ban küldi el a tartalmat, amit string-é konvertáltunk. ezt visszaalakítjuk JSON-é
    $formData = json_decode(file_get_contents("php://input"));

    // felülírjuk a tartalmat.
    $tracks->update(
        // megkeressük, mit írunk felül.
        function ($row) use ($id) {
            return $row['id'] === $id;
        },
        // itt pedig felüírjuk.
        function (&$row) use ($formData) {
            // ide több sor is kerülhet pl.: $row["jelszo"] = $formData->jelszo; $row["email"] = $formData->email; 
            $row['notes'] = $formData->notes;
        }
    );
    // feltesszük, hogy sikerült a mentés, így visszatértünk egy Status OK-kal, és egy semmitmondó üzenettel (mert nem jelenítjük meg sehol).
    // azért küldönk státuszt mert a javascriptben azt viszgáljuk, hogy van-e státusz és az OK-e?
    // a 200 az elején egy HTTP kód jelentése success OK
    sendResponse(200, array("status" => "OK", "msg" => "Sikeres mentés!"));
} else {
    // visszaküldünk egy 400-as hibakóddal egy hibaüzenetet, hogy az ID nem lehet üres.
    // 400 as HTTP-kód, 4xx-es kódok kliens oldali hibák, 400-as Bad Request, tehát most ez jelentheti azt, hogy nem volt ID mező, rossz volt a kérés.
    // 5xx as HTTP kódok, pedig a szerver oldali hibák.
    // részletek: https://httpstatuses.com/ 
    sendResponse(400, array("status" => "ERROR", "msg" => "ID nem lehet üres!"));
}



function sendResponse($statusCode, $dataArr)
{
    header('Content-Type: application/json'); // beállítjuk, hogy JSON megy vissza
    http_response_code($statusCode); // beállítjuk a státus kódot
    die(json_encode($dataArr)); // megöljük az oldalt, hogy további műveleteket ne végezzen el, és visszaküldük a JSON-t
}
